from django.db import models
from ckeditor.fields import RichTextField
from cogset.models import Quiz
from headlamp.models import Presentation

class Lesson(models.Model):	
	title = models.CharField(max_length=200)
	quiz = models.ForeignKey(Quiz)
	presentation = models.OneToOneField(Presentation)
	courses = models.ManyToManyField('Course')
	
	IMPORTANCE = ( (1, 'optional'),  (2, 'normal'), (3, 'essential') )
	importance = models.SmallIntegerField(choices=IMPORTANCE,default=2)
	
	def __unicode__(self):
		return self.title

class Resource(models.Model):
	title = models.CharField(max_length=200)
	description = RichTextField()
	
	def __unicode__(self):
		return self.title	
		
	class Meta:
		abstract = True
		
class FileResource(Resource):
	file = models.FileField(upload_to='resources', null=True, blank=True)
	lessons = models.ManyToManyField(Lesson)
	
class LinkResource(Resource):	
	link = models.URLField(max_length=200)
	lessons = models.ManyToManyField(Lesson)

class Course(models.Model): # Allows lessons to be part of one or more courses; ex. Academic
	title = models.CharField(max_length=200)
	slug = models.SlugField()
	
	def __unicode__(self):
		return self.slug

	