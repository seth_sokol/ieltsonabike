# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Lesson'
        db.create_table(u'cyclocomputer_lesson', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('quiz', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cogset.Quiz'])),
            ('presentation', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['headlamp.Presentation'], unique=True)),
            ('importance', self.gf('django.db.models.fields.SmallIntegerField')(default=2)),
        ))
        db.send_create_signal(u'cyclocomputer', ['Lesson'])

        # Adding M2M table for field courses on 'Lesson'
        db.create_table(u'cyclocomputer_lesson_courses', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('lesson', models.ForeignKey(orm[u'cyclocomputer.lesson'], null=False)),
            ('course', models.ForeignKey(orm[u'cyclocomputer.course'], null=False))
        ))
        db.create_unique(u'cyclocomputer_lesson_courses', ['lesson_id', 'course_id'])

        # Adding model 'FileResource'
        db.create_table(u'cyclocomputer_fileresource', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('ckeditor.fields.RichTextField')()),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'cyclocomputer', ['FileResource'])

        # Adding M2M table for field lessons on 'FileResource'
        db.create_table(u'cyclocomputer_fileresource_lessons', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fileresource', models.ForeignKey(orm[u'cyclocomputer.fileresource'], null=False)),
            ('lesson', models.ForeignKey(orm[u'cyclocomputer.lesson'], null=False))
        ))
        db.create_unique(u'cyclocomputer_fileresource_lessons', ['fileresource_id', 'lesson_id'])

        # Adding model 'LinkResource'
        db.create_table(u'cyclocomputer_linkresource', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('ckeditor.fields.RichTextField')()),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'cyclocomputer', ['LinkResource'])

        # Adding M2M table for field lessons on 'LinkResource'
        db.create_table(u'cyclocomputer_linkresource_lessons', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('linkresource', models.ForeignKey(orm[u'cyclocomputer.linkresource'], null=False)),
            ('lesson', models.ForeignKey(orm[u'cyclocomputer.lesson'], null=False))
        ))
        db.create_unique(u'cyclocomputer_linkresource_lessons', ['linkresource_id', 'lesson_id'])

        # Adding model 'Course'
        db.create_table(u'cyclocomputer_course', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
        ))
        db.send_create_signal(u'cyclocomputer', ['Course'])


    def backwards(self, orm):
        # Deleting model 'Lesson'
        db.delete_table(u'cyclocomputer_lesson')

        # Removing M2M table for field courses on 'Lesson'
        db.delete_table('cyclocomputer_lesson_courses')

        # Deleting model 'FileResource'
        db.delete_table(u'cyclocomputer_fileresource')

        # Removing M2M table for field lessons on 'FileResource'
        db.delete_table('cyclocomputer_fileresource_lessons')

        # Deleting model 'LinkResource'
        db.delete_table(u'cyclocomputer_linkresource')

        # Removing M2M table for field lessons on 'LinkResource'
        db.delete_table('cyclocomputer_linkresource_lessons')

        # Deleting model 'Course'
        db.delete_table(u'cyclocomputer_course')


    models = {
        u'cogset.quiz': {
            'Meta': {'object_name': 'Quiz'},
            'feedback': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'pub_status': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cyclocomputer.course': {
            'Meta': {'object_name': 'Course'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cyclocomputer.fileresource': {
            'Meta': {'object_name': 'FileResource'},
            'description': ('ckeditor.fields.RichTextField', [], {}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lessons': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cyclocomputer.Lesson']", 'symmetrical': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cyclocomputer.lesson': {
            'Meta': {'object_name': 'Lesson'},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cyclocomputer.Course']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importance': ('django.db.models.fields.SmallIntegerField', [], {'default': '2'}),
            'presentation': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['headlamp.Presentation']", 'unique': 'True'}),
            'quiz': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cogset.Quiz']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cyclocomputer.linkresource': {
            'Meta': {'object_name': 'LinkResource'},
            'description': ('ckeditor.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lessons': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['cyclocomputer.Lesson']", 'symmetrical': 'False'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.presentation': {
            'Meta': {'object_name': 'Presentation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['cyclocomputer']