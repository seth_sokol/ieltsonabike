from django.contrib import admin
from cyclocomputer.models import Lesson, FileResource, LinkResource, Course 

admin.site.register(Lesson)
admin.site.register(FileResource)
admin.site.register(LinkResource)
admin.site.register(Course)