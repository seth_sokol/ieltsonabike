from django.conf.urls import patterns, url
from django.views.generic import DetailView, ListView
from headlamp.models import Presentation, Slide, SlideSegment

urlpatterns = patterns('',
	# /headlamp/ or /presentations/
	url(r'^$',
		ListView.as_view(
			queryset = Presentation.objects.order_by('lesson','title')[:5],
			context_object_name = 'presentation_list',
			template_name = 'presentations/index.html'),
		name='index'),
	# /headlamp/4/show or /presentations/4/show
	url(r'^(?P<presentation_id>\d+)/show$', 'headlamp.views.show', name='show'),
)