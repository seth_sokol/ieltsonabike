from django.db import models
from ckeditor.fields import RichTextField

class Presentation(models.Model):
	title = models.CharField(max_length=200)
	audio = models.FileField(upload_to='audio', null=True, blank=True)
	
	def __unicode__(self):
		return self.title	
		
class Slide(models.Model):
	title = models.CharField(max_length=200)
	header = RichTextField(blank=True)
	footer = RichTextField(blank=True)
	presentation = models.ForeignKey(Presentation)
	slide_number = models.IntegerField()
	#style = models.ForeignKey(SlideStyle)
	
	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['slide_number']
		unique_together = (('presentation', 'slide_number'),)
	
class SlideSegment(models.Model):
	slide = models.ForeignKey(Slide)
	duration = models.IntegerField() # duration of slide segment in ms
	position = models.SmallIntegerField() # to order segments
	content = RichTextField(blank=True)
	subtitle = models.TextField(blank=True)

	def start_time (self): # cumulative elapsed time of other preceding segments
		previous_segments = SlideSegment.objects.filter(
			slide__presentation = self.slide.presentation #returns all segments in this presentation
		).exclude(
			slide__slide_number__gt = self.slide.slide_number #exclude later slides
		).exclude(
			slide = self.slide, position__gte = self.position #exclude later segments
		)
		return previous_segments.aggregate(models.Sum('duration'))['duration__sum'] #output sum duration values of all returned objects
	
	def __unicode__(self):
		return u"Slide %s Segment %s" % (str(self.slide.slide_number), str(self.position))

	class Meta:
		ordering = ['position']
		unique_together = (('slide', 'position'),)

#class SlideStyle(models.Model):
#	name = Charfield()
#	css = 