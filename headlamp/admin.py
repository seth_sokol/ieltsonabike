from django.contrib import admin
from headlamp.models import Presentation, Slide, SlideSegment 

class SlideSegmentInline(admin.StackedInline):
	model = SlideSegment
	extra = 1

class SlideAdmin(admin.ModelAdmin):
	fieldsets = (
		(None, {
			'fields': ('presentation', 'slide_number', 'title',)
		}),
		('Header/Footer', {
			'classes': ('collapse',),
			'fields': ('header', 'footer',)
		}),
	)
	inlines = [SlideSegmentInline]
	list_filter = ('presentation',)

#class SlideFilter(admin.SimpleListFilter):
#	title = 'slide'
#	parameter_name = 'slide'
#
#	def lookups(self, request, model_admin):
#		slides = set([ss.slide for ss in model_admin.model.objects.all()])
#		return [(s.id, s.title) for s in slides]
#
#	def queryset(self, request, queryset):
#		if self.value():
#			return queryset.filter(slide__id__exact = self.value()) 
#		else:
#			return queryset

class SlideSegmentAdmin(admin.ModelAdmin):
	exclude =()
	list_filter = ('slide__presentation', 'slide')

admin.site.register(Presentation)
admin.site.register(Slide, SlideAdmin)
admin.site.register(SlideSegment, SlideSegmentAdmin)