from django.shortcuts import render, get_object_or_404
from headlamp.models import Presentation

def show(request, presentation_id):
	presentation = get_object_or_404(Presentation, pk=presentation_id)
	return render(request, 'presentations/show.html', {'presentation': presentation})
