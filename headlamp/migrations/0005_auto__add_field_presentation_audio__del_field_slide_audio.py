# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Presentation.audio'
        db.add_column(u'headlamp_presentation', 'audio',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Slide.audio'
        db.delete_column(u'headlamp_slide', 'audio')


    def backwards(self, orm):
        # Deleting field 'Presentation.audio'
        db.delete_column(u'headlamp_presentation', 'audio')

        # Adding field 'Slide.audio'
        db.add_column(u'headlamp_slide', 'audio',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)


    models = {
        u'headlamp.presentation': {
            'Meta': {'object_name': 'Presentation'},
            'audio': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.slide': {
            'Meta': {'ordering': "['slide_number']", 'unique_together': "(('presentation', 'slide_number'),)", 'object_name': 'Slide'},
            'footer': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'header': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['headlamp.Presentation']"}),
            'slide_number': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.slidesegment': {
            'Meta': {'ordering': "['position']", 'unique_together': "(('slide', 'position'),)", 'object_name': 'SlideSegment'},
            'content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'duration': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {}),
            'slide': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['headlamp.Slide']"}),
            'subtitle': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['headlamp']