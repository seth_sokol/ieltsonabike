# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SlideSegment.subtitle'
        db.add_column(u'headlamp_slidesegment', 'subtitle',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Deleting field 'Slide.subtitle'
        db.delete_column(u'headlamp_slide', 'subtitle')


    def backwards(self, orm):
        # Deleting field 'SlideSegment.subtitle'
        db.delete_column(u'headlamp_slidesegment', 'subtitle')


        # User chose to not deal with backwards NULL issues for 'Slide.subtitle'
        raise RuntimeError("Cannot reverse this migration. 'Slide.subtitle' and its values cannot be restored.")

    models = {
        u'headlamp.presentation': {
            'Meta': {'object_name': 'Presentation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.slide': {
            'Meta': {'object_name': 'Slide'},
            'audio': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'footer': ('ckeditor.fields.RichTextField', [], {}),
            'header': ('ckeditor.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['headlamp.Presentation']"}),
            'slide_number': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.slidesegment': {
            'Meta': {'ordering': "['position']", 'object_name': 'SlideSegment'},
            'content': ('ckeditor.fields.RichTextField', [], {}),
            'duration': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {}),
            'slide': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['headlamp.Slide']"}),
            'subtitle': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['headlamp']