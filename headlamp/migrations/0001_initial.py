# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Presentation'
        db.create_table(u'headlamp_presentation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'headlamp', ['Presentation'])

        # Adding model 'Slide'
        db.create_table(u'headlamp_slide', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('subtitle', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('header', self.gf('ckeditor.fields.RichTextField')()),
            ('footer', self.gf('ckeditor.fields.RichTextField')()),
            ('presentation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['headlamp.Presentation'])),
            ('slide_number', self.gf('django.db.models.fields.IntegerField')()),
            ('audio', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'headlamp', ['Slide'])

        # Adding model 'SlideSegment'
        db.create_table(u'headlamp_slidesegment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slide', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['headlamp.Slide'])),
            ('duration', self.gf('django.db.models.fields.IntegerField')()),
            ('position', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('content', self.gf('ckeditor.fields.RichTextField')()),
        ))
        db.send_create_signal(u'headlamp', ['SlideSegment'])


    def backwards(self, orm):
        # Deleting model 'Presentation'
        db.delete_table(u'headlamp_presentation')

        # Deleting model 'Slide'
        db.delete_table(u'headlamp_slide')

        # Deleting model 'SlideSegment'
        db.delete_table(u'headlamp_slidesegment')


    models = {
        u'headlamp.presentation': {
            'Meta': {'object_name': 'Presentation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.slide': {
            'Meta': {'object_name': 'Slide'},
            'audio': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'footer': ('ckeditor.fields.RichTextField', [], {}),
            'header': ('ckeditor.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['headlamp.Presentation']"}),
            'slide_number': ('django.db.models.fields.IntegerField', [], {}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'headlamp.slidesegment': {
            'Meta': {'ordering': "['position']", 'object_name': 'SlideSegment'},
            'content': ('ckeditor.fields.RichTextField', [], {}),
            'duration': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {}),
            'slide': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['headlamp.Slide']"})
        }
    }

    complete_apps = ['headlamp']