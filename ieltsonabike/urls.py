from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^cogset/', include('cogset.urls')),
    url(r'^quizzes/', include('cogset.urls', namespace="quizzes")),
    url(r'^presentations/', include('headlamp.urls', namespace="presentations")),
    
    # Examples:
	# url(r'^$', 'ieltsonabike.views.home', name='home'),
    # url(r'^ieltsonabike/', include('ieltsonabike.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    # CKEditor URL for rich text fields
    (r'^ckeditor/', include('ckeditor.urls')),
    
)

# Debug Media File Server
if settings.DEBUG:
# static files (images, css, javascript, etc.)
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))
