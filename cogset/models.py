from django.db import models
from django.contrib.auth.models import User

class Quiz(models.Model):
	PUB_STATUS = ( 
		 ('D', 'draft'), # unfinished draft
		 ('P', 'publish'), # published, finished
		 ('T', 'trash'), # deleted
	)
	
	title = models.CharField(max_length=200)
	pub_status= models.CharField(max_length=1, choices=PUB_STATUS)
	pub_date = models.DateTimeField('date published')
	feedback = models.TextField(blank=True, default="")  # to provide user some post quiz comment 
	
	def __unicode__(self):
		return self.title

	class Meta:
		verbose_name_plural = "Quizzes"

class Question(models.Model):
	title = models.CharField(max_length=200)
	hint = models.CharField(max_length=200, blank=True, default="")
	quiz = models.ForeignKey(Quiz)
	
	def __unicode__(self):
		return self.title

class Answer(models.Model):
	title = models.CharField(max_length=200)
	question = models.ForeignKey(Question)
	feedback = models.TextField(blank=True, default="") # to provide user some comment on his answer
	score = models.DecimalField(max_digits=2, decimal_places=1) # full score is 1, half in 0.5, etc
	is_correct = models.BooleanField()
	
	def __unicode__(self):
		return self.title
	
	class Meta:
		order_with_respect_to = 'question' # uses get/set_answer_order() methods

	def score_as_percentage(self):
		return "{0:.0f}%".format(self.score*100)
	
#	def _order_as_int(self): # outputs as integer the long _order value created by order_wrt
#		return int(self._order)
#		
#	position = property(_order_as_int) # position property for grappelli
		
class Response(models.Model):
	user = models.ForeignKey(User)
	question = models.ForeignKey(Question)
	answer = models.ForeignKey(Answer)
	
	def __unicode__(self):
		return u"response id %s from user %s" % (str(self.id), self.user.username)
	
	def _score(self):
		return self.answer.score
			
	score = property(_score)
