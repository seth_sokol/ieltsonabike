from django.conf.urls import patterns, url
from django.views.generic import DetailView, ListView
from cogset.models import Quiz, Question, Response

urlpatterns = patterns('',
	#ex: /quizzes/
	url(r'^$',
		ListView.as_view(
			queryset = Quiz.objects.order_by('-pub_date')[:5],
			context_object_name = 'latest_quiz_list',
			template_name = 'quizzes/index.html'),
		name='index'),
	#ex: /quizzes/5
	url(r'^(?P<pk>\d+)/$',
		DetailView.as_view(
			model=Quiz,
			template_name='quizzes/detail.html'),
		name='detail'),
	# ex: /quizzes/question/5
	url(r'^question/(?P<pk>\d+)/$',
		DetailView.as_view(
			model=Question,
			template_name='quizzes/question.html'),
		name='question'),
	# handles form response
	url(r'^respond/(?P<question_id>\d+)$', 'cogset.views.respond', name='respond'),
	# ex: quizzes/results/5
	url(r'^results/(?P<pk>\d+)$',
		DetailView.as_view(
			model=Response,
			template_name='quizzes/results.html'),
		name='results'),
)