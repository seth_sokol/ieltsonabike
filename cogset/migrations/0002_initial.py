# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Quiz'
        db.create_table(u'cogsetquiz_quiz', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('pub_status', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('feedback', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal(u'cogsetquiz', ['Quiz'])

        # Adding model 'Question'
        db.create_table(u'cogsetquiz_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('hint', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('quiz', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cogsetquiz.Quiz'])),
        ))
        db.send_create_signal(u'cogsetquiz', ['Question'])

        # Adding model 'Answer'
        db.create_table(u'cogsetquiz_answer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cogsetquiz.Question'])),
            ('score', self.gf('django.db.models.fields.DecimalField')(max_digits=2, decimal_places=1)),
            ('feedback', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('is_correct', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'cogsetquiz', ['Answer'])


    def backwards(self, orm):
        # Deleting model 'Quiz'
        db.delete_table(u'cogsetquiz_quiz')

        # Deleting model 'Question'
        db.delete_table(u'cogsetquiz_question')

        # Deleting model 'Answer'
        db.delete_table(u'cogsetquiz_answer')


    models = {
        u'cogsetquiz.answer': {
            'Meta': {'ordering': "(u'_order',)", 'object_name': 'Answer'},
            '_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'feedback': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_correct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cogsetquiz.Question']"}),
            'score': ('django.db.models.fields.DecimalField', [], {'max_digits': '2', 'decimal_places': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cogsetquiz.question': {
            'Meta': {'object_name': 'Question'},
            'hint': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quiz': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cogsetquiz.Quiz']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cogsetquiz.quiz': {
            'Meta': {'object_name': 'Quiz'},
            'feedback': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'pub_status': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['cogsetquiz']