from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from cogset.models import Quiz, Question, Answer, Response
    
def respond(request, question_id): # processes submitted answer 
	q = get_object_or_404(Question, pk=question_id)
	try:
		selected_answer = q.answer_set.get(pk=request.POST['answer'])
	except (KeyError, Answer.DoesNotExist):
		# Redisplay the question form.
		return render(request, 'quizzes/question.html', {
			'question': q,
			'error_message': "You didn't select an answer.",
    })
	else:
		# answer is submitted so a response object is created and saved
		r = Response(user=request.user, question=q, answer=selected_answer)
		r.save()
		return HttpResponseRedirect(reverse('quizzes:results', args=(r.id,)))
        
def results(request, response_id):
    response = get_object_or_404(Response, pk=response_id)
    return render(request, 'quizzes/results.html', {'response': response})