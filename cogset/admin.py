from django.contrib import admin
from cogset.models import Quiz, Question, Answer, Response

class AnswerInline(admin.TabularInline):
	model = Answer
#	extra = 2
#	sortable_field_name= "position"
#	fieldsets = [
#		(None, {'fields': ['title', 'score']}),
#		(None, {'fields': ['feedback']}),
#	]

class QuestionInline(admin.StackedInline):
	model = Question
	exclude = ('hint',)
	extra = 1

class QuestionAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,         {'fields': ['title']}),
		('Quiz',       {'fields': ['quiz']}),
		#('Answers',    {'fields': ['answers', 'correct_answer']}),
		('Add a hint', {'fields': ['hint'], 'classes': ['collapse']}),
	]
	inlines = [AnswerInline]

class QuizAdmin(admin.ModelAdmin):
	list_filter = ['pub_date']
	fieldsets = [
		(None,                 {'fields': ['title']}),
		('Publication Status', {'fields': ['pub_date', 'pub_status']}),
		(None,                 {'fields': ['feedback']}),
	]
	inlines = [QuestionInline]
		
class AnswerAdmin(admin.ModelAdmin):
	def position(obj):
		return obj._order

	list_display = ('title', 'score', 'is_correct')
	list_editable = ('score', 'is_correct',)
	list_filter = ('question__quiz','question')
	fieldsets = [
		(None, {'fields': ['question', 'title', 'score', 'is_correct']}),
		(None, {'fields': ['feedback']}),
	]	
	
admin.site.register(Quiz, QuizAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Response)